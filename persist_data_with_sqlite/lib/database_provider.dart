import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:path/path.dart';

class DatabaseProvider {
  static Future<Database>? _database;
  static Future<Database> get database {
    return _database ?? initDB();
  }

  static initDB() async {
    _database = openDatabase(
      join(await getDatabasesPath(), 'doggie_database.db'),
      onCreate: (db, version) {
        return db.execute(
            'CREATE TABLE dogs(id INTEGER PRIMARY KEY,name TEXT.age INTEGER)');
      },
      version: 1,
    );
    return _database;
  }
}
