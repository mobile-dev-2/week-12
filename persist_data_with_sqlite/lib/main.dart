import 'package:flutter/material.dart';
import 'dart:async';
import 'package:path/path.dart';
import 'package:persist_data_with_sqlite/database_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'dog.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  Future<void> insertDog(Dog dog) async {
    final db = await DatabaseProvider.database;
    await db.insert('dogs', dog.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  var fido = Dog(id: 0, name: 'Fido', age: 35);
  var dido = Dog(id: 1, name: 'Dido', age: 20);
  await insertDog(fido);
  await insertDog(dido);

  Future<List<Dog>> dogs() async {
    final db = await DatabaseProvider.database;
    return Dog.toList(await db.query('dogs'));
  }

  print(await dogs());

  Future<void> updateDog(Dog dog) async {
    final db = await DatabaseProvider.database;
    await db.update(
      'dogs',
      dog.toMap(),
      where: 'id = ?',
      whereArgs: [dog.id],
    );
  }

  fido = Dog(
    id: fido.id,
    name: fido.name,
    age: fido.age + 7,
  );

  await updateDog(fido);
  print(await dogs());

  Future<void> deleteDog(int id) async {
    final db = await DatabaseProvider.database;
    await db.delete(
      'dogs',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  await deleteDog(0);
  print(await dogs());
}
